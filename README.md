# Panduan Isi Laporan Pajak

## Daftar Isi

- [1. Buat NPWP](#1-buat-npwp)
- [2. Daftar EFIN](#2-daftar-efin)
  * [Saat pandemi](#saat-pandemi)
    + [KPP Pratama Pondok Aren](#kpp-pratama-pondok-aren)
    + [KPP yang lain](#kpp-yang-lain)
  * [Sebelum pandemi](#sebelum-pandemi)
- [3. Registrasi DJP Online](#3-registrasi-djp-online)
- [4. Isi SPT Online](#4-isi-spt-online)
  * [Penghasilan Kurang dari Rp 60 juta per tahun](#penghasilan-kurang-dari-rp-60-juta-per-tahun)
  * [Penghasilan Setara atau Lebih dari Rp 60 juta per tahun](#penghasilan-setara-atau-lebih-dari-rp-60-juta-per-tahun)
  * [Cara Mengisi SPT bila Pindah Kerja](#cara-mengisi-spt-bila-pindah-kerja)

<small><i><a href='http://ecotrust-canada.github.io/markdown-toc/'>Table of contents generated with markdown-toc</a></i></small>

## 1. Buat NPWP

- [Cara Daftar NPWP Online, Seperti Apa Prosesnya?](https://www.cermati.com/artikel/cara-daftar-npwp-online-seperti-apa-prosesnya)

## 2. Daftar EFIN

### Saat pandemi

#### KPP Pratama Pondok Aren

1) Kirim email ke `kpp.453@pajak.go.id` dengan subjek `AKTIVASI EFIN`
2) Isi email:
    - NPWP
    - nama sesuai KTP
    - NIK
    - e-mail (klo bisa email yang sama pas e-registrasi NPWP)
    - no. HP
 3) Lampirkan selfie dengan KTP dan NPWP. Pastikan gambar (termasuk angka-angka di kartu) terlihat jelas.

#### KPP yang lain

![Prosedur daftar EFIN melalui email](./registrasi-efin-email.png)
Sumber: [Kompas](https://money.kompas.com/read/2022/01/11/154500926/simak-ini-dua-cara-daftar-efin-online)<br>
Lihat daftar email di sini: [Unit Kerja](https://pajak.go.id/unit-kerja)

### Sebelum pandemi

- [Formulir permohonan EFIN](https://www.pajak.go.id/id/formulir-permohonan-efin)

## 3. Registrasi DJP Online

- [Cara Registrasi DJP Online dan Cara Mendapatkan e-FIN](https://www.cermati.com/artikel/cara-registrasi-djp-online-dan-cara-mendapatkan-e-fin)

## 4. Isi SPT Online

CATATAN:
```
Sebelum isi SPT online, minta bukti potong ke kantor.
Lembaran bukti potong 1721 A1 untuk karyawan swasta atau untuk PNS bukti potong 1721 A2.
```

Panduan dari Talenta bisa dilihat di [sini](./panduan-lapor-spt-pribadi-maret-2021.pdf).

### Penghasilan Kurang dari Rp 60 juta per tahun

- [Cara Isi dan Lapor SPT Pajak Online atau E-Filing 1770 SS - Cermati](https://www.cermati.com/artikel/cara-isi-dan-lapor-spt-pajak-online-atau-e-filing-1770-ss)
- Panduan video: [Tutorial Pengisian SPT 1770 SS Melalui E-Filing](https://www.youtube.com/watch?v=4_A7uBpf6q4)

### Penghasilan Setara atau Lebih dari Rp 60 juta per tahun

- [Cara Mengisi dan Lapor SPT Pajak Online atau E-Filing 1770 S - Cermati](https://www.cermati.com/artikel/cara-mengisi-dan-lapor-spt-pajak-online-atau-e-filing-1770-s)
- Panduan video
    - [Tutorial Pengisian SPT 1770 S Melalui e-Filing](https://www.youtube.com/watch?v=jLSARSW3vzk)
    - [Cara Mengisi efiling diatas 60 Juta || DPJ Online Pajak 2020](https://www.youtube.com/watch?v=i4PgVYIAGLs)

### Cara Mengisi SPT bila Pindah Kerja

- [Cara Mengisi SPT bila Pindah Kerja di 2 Perusahaan dalam Setahun - Cermati](https://www.cermati.com/artikel/cara-mengisi-spt-bila-pindah-kerja-di-2-perusahaan-dalam-setahun)
